package core.transaction;

import java.util.Calendar;
import java.util.Date;

public class Transaction {

    private double amount;
    private Date when = Calendar.getInstance().getTime();

    public Transaction(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public Date getDate() {
        return when;
    }

}
