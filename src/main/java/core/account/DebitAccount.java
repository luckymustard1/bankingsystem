package core.account;

public class DebitAccount extends Account {

    public DebitAccount(int accountNumber, int sortCode, String firstName, String lastName, double startingBalance) {
        super(accountNumber, sortCode, firstName, lastName, startingBalance);
    }

    @Override
    public boolean debit(double amountToDebit) {
        boolean amountAvailable = false;
        if (amountToDebit <= getAvailableBalance()) {
            amountAvailable = super.debit(amountToDebit);
        }
        return amountAvailable;
    }

    @Override
    public boolean credit(double amountToCredit) {
        return super.credit(amountToCredit);
    }

}
