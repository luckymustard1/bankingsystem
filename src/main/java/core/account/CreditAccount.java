package core.account;

import core.InterestRate;

public class CreditAccount extends Account {

    private double overdraftLimit, overdraftAvailable;
    private InterestRate accountType;

    public CreditAccount(int accountNumber, int sortCode, String firstName, String lastName, double startingBalance, double overdraftLimit, InterestRate accountType) {
        super(accountNumber, sortCode, firstName, lastName, startingBalance);
        this.overdraftLimit = overdraftLimit;
        updateAvailableOverdraft();
        this.accountType = accountType;
    }

    private void updateAvailableOverdraft() {
        overdraftAvailable = overdraftLimit + getAvailableBalance();
    }

    public double getOverdraftLimit() {
        return overdraftLimit;
    }

    public double getOverdraftAvailable() {
        return overdraftAvailable;
    }

    public double getInterestRate() {
        return accountType.getInterestRate();
    }

    @Override
    public boolean debit(double amountToDebit) {
        boolean enoughOverdraftAvailable = false;
        if (amountToDebit <= overdraftAvailable) {
            double interest = 0;
            if (getAvailableBalance() - amountToDebit < 0) {
                interest = (amountToDebit - getAvailableBalance()) * (1 - Math.pow(1 + getInterestRate(), (double) 1 / 12)) * -1;
            }
            enoughOverdraftAvailable = super.debit(amountToDebit + interest);
            updateAvailableOverdraft();
        }
        return enoughOverdraftAvailable;
    }

    @Override
    public boolean credit(double amountToCredit) {
        boolean success = super.credit(amountToCredit);
        updateAvailableOverdraft();
        return success;
    }

    private void addInterest() {
        if (getAvailableBalance() < 0) {
            debit(getAvailableBalance() * (1 - Math.pow(1 + getInterestRate(), (double) 1 / 12)) * -1);
        }
    }

}
