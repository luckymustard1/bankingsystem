package core.account;

import core.InterestRate;

public class CreditCardAccount extends Account {

    private double creditLimit, availableCredit;
    private InterestRate accountType;

    public CreditCardAccount(int accountNumber, int sortCode, String firstName, String lastName, double creditLimit, InterestRate accountType) {
        super(accountNumber, sortCode, firstName, lastName, 0);
        this.creditLimit = creditLimit;
        this.accountType = accountType;
    }

    private void updateAvailableCredit() {
        availableCredit = creditLimit - getAvailableBalance();
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public double getAvailableCredit() {
        return availableCredit;
    }

    public double getInterestRate() {
        return accountType.getInterestRate();
    }

    @Override
    public boolean debit(double amountToDebit) {
        boolean enoughCreditAvailable = false;
        if (amountToDebit <= availableCredit) {
            enoughCreditAvailable = super.debit(amountToDebit);
            updateAvailableCredit();
        }
        return enoughCreditAvailable;
    }

    @Override
    public boolean credit(double amountToCredit) {
        boolean amountToCreditIsLowerThanAvailable = false;
        if (amountToCredit <= availableCredit) {
            amountToCreditIsLowerThanAvailable = super.credit(amountToCredit);
            updateAvailableCredit();
        }
        return amountToCreditIsLowerThanAvailable;
    }

    private void addInterest() {
        if (getAvailableBalance() < 0) {
            credit(getAvailableBalance() * (1 - Math.pow(1 + getInterestRate(), 1 / 12)));
        }
    }
}
