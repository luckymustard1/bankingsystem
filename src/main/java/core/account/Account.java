package core.account;

import core.transaction.Transaction;

import java.util.ArrayList;
import java.util.List;

public abstract class Account {

    private int accountNumber, sortCode;
    private String firstName, lastName;
    private final List<Transaction> transactions = new ArrayList<>();
    private double balance;

    public Account(int accountNumber, int sortCode, String firstName, String lastName, double startingBalance) {
        this.accountNumber = accountNumber;
        this.sortCode = sortCode;
        this.firstName = firstName;
        this.lastName = lastName;
        credit(startingBalance);
    }

    public double getAvailableBalance() {
        return balance;
    }

    public double getCurrentBalance() {
        double currentBalance = 0;
        for (Transaction t : transactions) {
            currentBalance += t.getAmount();
        }
        return currentBalance;
    }

    public boolean credit(double amountToCredit) {
        transactions.add(new Transaction(amountToCredit));
        balance += amountToCredit;
        return true;
    }

    public boolean debit(double amountToDebit) {
        transactions.add(new Transaction(amountToDebit * -1));
        balance -= amountToDebit;
        return true;
    }

}
