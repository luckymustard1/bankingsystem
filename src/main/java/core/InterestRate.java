package core;

public enum InterestRate {
    HOME_LOAN(0.219),
    BUSINESS_LOAN(0.173);

    private final double interestRate;

    InterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public double getInterestRate() {
        return interestRate;
    }
}
