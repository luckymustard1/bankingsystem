package core.tests;

import core.InterestRate;
import core.account.Account;
import core.account.CreditAccount;
import core.account.DebitAccount;
import core.transaction.Transaction;

public class MainUnit {

    public static void main(String[] args) {

        test_transactions();

        test_does_balance_match_after_print();

        test_interest_rate_is_correct_in_credit_account();

    }

    static void test_transactions() {
        Transaction exampleTransaction = new Transaction(30.2);
        System.out.println(exampleTransaction.getAmount());
        System.out.println(exampleTransaction.getDate());
    }

    static void test_interest_rate_is_correct_in_credit_account() {
        //arrange
        double startingBalance = 0;
        double overdraftLimit = 3000;
        InterestRate exampleRate = InterestRate.BUSINESS_LOAN;
        double amountToDebit = 500;
        double interestCharge = (amountToDebit - startingBalance) * (1 - Math.pow(1 + exampleRate.getInterestRate(), (double) 1 / 12));
        Account cAccount = new CreditAccount(12345678, 123456, "", "",
                startingBalance, overdraftLimit, exampleRate);
        //act
        cAccount.debit(amountToDebit);
        //assert
        if (startingBalance - amountToDebit + interestCharge == cAccount.getAvailableBalance()) {
            System.out.println("test_interest_rate_is_correct_in_credit_account:PASSED");
        } else {
            System.out.println("test_interest_rate_is_correct_in_credit_account:FAILED");
        }
    }

    static void test_does_balance_match_after_print() {

        //arrange
        double balance = 5.02;
        //act
        DebitAccount dacc =
                new DebitAccount(12345678, 123456, "", "", balance);
        //assert
        if (balance == dacc.getAvailableBalance() && balance == dacc.getCurrentBalance()) {
            System.out.println("test_does_balance_match_after_print:PASSED");
        } else {
            System.out.println("test_does_balance_match_after_print:FAILED");
        }

        //arrange
        double amountToCredit = 78274.90;
        balance += amountToCredit;
        //act
        dacc.credit(amountToCredit);
        //assert
        if (balance == dacc.getAvailableBalance() && balance == dacc.getCurrentBalance()) {
            System.out.println("test_does_balance_match_after_print:PASSED");
        } else {
            System.out.println("test_does_balance_match_after_print:FAILED");
        }

        //arrange
        double amountToDebit = 399.99;
        balance -= amountToDebit;
        //act
        dacc.debit(amountToDebit);
        //assert
        if (balance == dacc.getAvailableBalance() && balance == dacc.getCurrentBalance()) {
            System.out.println("test_does_balance_match_after_print:PASSED");
        } else {
            System.out.println("test_does_balance_match_after_print:FAILED");
        }

        //arrange
        double amountToCredit2 = 98766.32;
        double amountToDebit2 = 212.63;
        balance += amountToCredit2 - amountToDebit2;
        //act
        dacc.credit(amountToCredit2);
        dacc.debit(amountToDebit2);
        //assert
        if (balance == dacc.getAvailableBalance() && balance == dacc.getCurrentBalance()) {
            System.out.println("test_does_balance_match_after_print:PASSED");
        } else {
            System.out.println("test_does_balance_match_after_print:FAILED");
        }
    }

}
