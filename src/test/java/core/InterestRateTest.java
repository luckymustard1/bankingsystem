package core;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InterestRateTest {

    @Test
    void getInterestRate() throws NoSuchFieldException, IllegalAccessException {
        InterestRate interestRate = InterestRate.HOME_LOAN;
        double rate = interestRate.getInterestRate();
        final Field field = interestRate.getClass().getDeclaredField("interestRate");
        field.setAccessible(true);
        field.set(interestRate, rate);
        assertEquals(rate, interestRate.getInterestRate());
    }
}